import Ember from 'ember';

export function firstName(params/*, hash*/) {
    return params[0].split(' ')[0];
}

export default Ember.Helper.helper(firstName);
